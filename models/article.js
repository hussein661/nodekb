let mongoose = require('mongoose');

//Article Schema
let articleSchema = mongoose.Schema({
    title:{
        type:String,
        required:true
    },
    author:{
        type: String,
    },
    body:{
        type:String,
        required:false
    }
    }
);

let Article = module.exports = mongoose.model('Article', articleSchema);

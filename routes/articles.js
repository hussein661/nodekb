const express = require('express');
const router = express.Router();

// Article Model
let Article = require('../models/article');
let User = require('../models/user')



// load add articles
router.get('/add',ensureAuthenticated, function (req,res){
  res.render('add_article', {
    title:'Add Article',
    
})
})


//add article

router.post('/add',function(req,res) {

   const title = req.body.title;
   const author = req.user._id;
   const body = req.body.body;

   req.checkBody('title', 'title is required').notEmpty();
  // req.checkBody('author', 'author is required').notEmpty();
   req.checkBody('body', 'body is required').notEmpty();

   let errors = req.validationErrors();
   if(errors){
     res.render('add_article', {
       errors:errors
     } )
   } else {

     let article = new Article({

       title : title,
       author:author,
       body :body
     });

   article.save(function(err){
     if(err){
       console.log(err);
     } else {
       req.flash('notify','Article Added Successfully !')
       res.redirect('/');
     }
 })  


   }})


// Load Edit Form
router.get('/edit/:id', ensureAuthenticated, function(req, res){
  Article.findById(req.params.id, function(err, article){
    if(article.author != req.user._id){
      req.flash('notauth','Not authorized')
      res.redirect('/')
    }
    res.render('edit_article', {
      title:'Edit Article',
      article:article
    });
  });
})


// // lets try this
// const { check, validationResult } = require('express-validator/check'); 
// const { matchedData, sanitize } = require('express-validator/filter'); 
// //you don't need this in this example 
// //no middleware 
// router.post('/add', [ check('title').isLength({min:1}).trim().withMessage('Title required'),
//                       check('author').isLength({min:1}).trim().withMessage('Author required'),
//                       check('body').isLength({min:1}).trim().withMessage('Body required') ],
// (req,res,next)=>{ 
// let article = new Article({ 
//     title:req.body.title,
//     author:req.body.author,
//     body:req.body.body
//   }); 
// const errors = validationResult(req);
// if (!errors.isEmpty()) {
//       console.log(errors);
//       res.render('add_article', { 
//         article:article,
//         errors: errors.mapped() }); 
// }else{ 
//   article.title = req.body.title;
//   article.author = req.body.author;
//   article.body = req.body.body;

//   article.save(err=>{ 
//     if(err)throw err;
//     req.flash('success','Article Added');
//     res.redirect('/'); });
//   } }); 

 // Update Submit POST Route
 router.post('/edit/:id', function(req, res){
   let article = {};
   article.title = req.body.title;
   article.author = req.user._id;
   article.body = req.body.body;

   let query = {_id:req.params.id}

   Article.update(query, article, function(err){
     if(err){
       console.log(err);
     } else {
       req.flash('notify','Article Updated Successfully !')
       res.redirect('/');
     }
   });
 })

// Delete Article
router.delete('/:id', function(req, res){
  let query = {_id:req.params.id}
  Article.findById(req.params.id, function(err, article){
      Article.remove(query, function(err){
        if(err){
          console.log(err);
        }
        req.flash('notify','Article Deleted Successfully !')
        res.send('Success');
        
        
      })
    })})




  // access control
  function ensureAuthenticated(req,res,next){
    if(req.isAuthenticated()){
      return next();
    }else {
      req.flash('asklogin', 'Please login')
      res.redirect('/users/login')
    }
  }

  // Get Single Article
router.get('/:id', function(req, res){
  Article.findById(req.params.id, function(err, article){
    User.findById(article.author,function(err,user){
      res.render('article', {
        article:article,
        author : user.name
      });
    })
     
    });
  });


module.exports = router;

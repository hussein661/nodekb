$(document).ready(function(){
    $('.delete-article').on('click', function(e){
        var r = confirm("are you sure ?")
        if(r==true) {
            $target = $(e.target);
            const id = $target.attr('data-id');
            $.ajax({
                type:'DELETE',
                url: '/articles/' +id,
                success:function(response){
                    window.location.href='/'
                    
                },
                error : function(err){
                    console.log(err);
                }
            })
        } 
     
    });
});